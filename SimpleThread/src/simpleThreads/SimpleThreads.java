package simpleThreads;

public class SimpleThreads {

	static void threadMessage(String message) {
		String threadName = Thread.currentThread().getName();
		System.out.format("%s: %s%n", threadName, message);
	}

	private static class MessageLoop
	implements Runnable {
		public void run() {
			String importantInfo[] = {
					"Giro giro tondo",
					"Casca il mondo",
					"Casca la terra",
					"Tutti giù per terra!"
			};
			try {
				for (int i = 0;
						i < importantInfo.length;
						i++) {
					Thread.sleep(400);
					threadMessage(importantInfo[i]);
				}
			} catch (InterruptedException e) {
				threadMessage("Finito!");
			}
		}
	}

	public static void main(String args[])
			throws InterruptedException {
		long patience = 1000 * 60 * 60;
		if (args.length > 0) {
			try {
				patience = Long.parseLong(args[0]) * 1000;
			} catch (NumberFormatException e) {
				System.err.println("L'argomento deve essere intero.");
				System.exit(1);
			}
		}
		threadMessage("Avvio del thread MessageLoop");
		long startTime = System.currentTimeMillis();
		Thread t = new Thread(new MessageLoop());
		t.start();

		threadMessage("Attendo la fine di MessageLoop");
		while (t.isAlive()) {
			threadMessage("Aspetto...");
			t.join(1000);
			if (((System.currentTimeMillis() - startTime) > patience)
					&& t.isAlive()) {
				threadMessage("Sono stufo!");
				t.interrupt();
				t.join();
			}
		}
		threadMessage("Finalmente!");
	}
}
